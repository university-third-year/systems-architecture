#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>

int main(int argc, char **argv){
    pid_t pid;
    pid_t pid_fin[5];//array para guardar el nº de proceso de 5 hijos
    int status[5];//array para guardar el estado de 5 hijos
    for(int i=0; i<5;i++){//bucle que itera 5 veces
        pid=fork();//crear hijo
        if(pid<0){//Lanzar error si el hijo no se crea correctamente
            fprintf(stderr,"Fork Failed\n");
            exit(EXIT_FAILURE);
        } else if(pid==0){//Si es un hijo, ejecutar proceso.c
            printf("soy el hijo %i\n", getpid());
            execvp("./proceso",argv);
        } else{//Si es un padre
            pid_fin[i]=wait(&status[i]);//guardar pid y estatus de terminación del hijo
            if (pid_fin[i]==-1){
                perror(strerror(errno));
            }
            if(kill(pid_fin[i],SIGKILL)<0){//Matar al hijo
                perror(strerror(errno));
            }
        }
    }
    printf("Soy %d y mis hijos murieron en este orden con estos estados:\n ", getpid());
    for (int i=0;i<5;i++){
        printf("%d %d ; ", pid_fin[i], WEXITSTATUS(status[i]));
    }
    //printf("\n Pd.: ningún padre debería enterrar a sus hijos.\n");
    exit(EXIT_SUCCESS);
}


