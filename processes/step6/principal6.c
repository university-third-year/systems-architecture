#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>

#define FIFO_FILE "MYFIFO"

struct entrada_tabla{
    int arg;
    pid_t pid;
    char last_status[10];
    int ejec;
    int status;
};

void signal_handler(int signo);
//actualizar iteraciones
void update_iter(struct entrada_tabla entradas[5], pid_t pid_son);
//revisar si un proceso esta muerto
bool isdead(pid_t pid, pid_t dead_son[5]);
//revisar que todos estan muertos
bool alldead(pid_t dead_son[5]);
//reordenar los procesos
void reorder(pid_t dead_son[5], pid_t pid_son[5]);
//inserta un pid en el array de procesos muertos
void insertdead(pid_t pid, pid_t dead_son[5]);
//cambiar current al siguiente a ejecutar
int updatecurrent(int son, int n_proceso, pid_t pid_son, struct entrada_tabla entradas[5]);
//comprobar que un pid en especifico coincide con los 5 que utilizamos
bool exists(pid_t pid, struct entrada_tabla entradas[5]);
//busca que posicion ocupa un pid en pid_son
int searchindex(pid_t pid);


int alarma=0;
int fin=0; //flag para detectar CTRL+C
int quit=0; //flag para detectar CTRL+"\"
int iter_sons[5] = {0,0,0,0,0}; //array de contadores de # iteraciones para los procesos hijos

/*array de pids que tenemos, lo usamos para establecer el orden de ejecución moviendo
  los que van muriendo al final y reordenando el resto para que queden seguidos al 
  principio los que siguen vivos*/
pid_t pid_son[5]={0,0,0,0,0};
//guardamos los que van muriendo
pid_t dead_son[5]={0,0,0,0,0};
//ultimo en ejecucion
pid_t pid_last = 0;
//numero de procesos muertos
int ndead=0;

int main(int argc, char **argv){

    FILE *fp;

    pid_t pid=0;
    pid_t father=getpid();
    
    int pid_fin[5]={0,0,0,0,0};
    int status[5]={0,0,0,0,0};
    //struct usado para hacer los prints necesarios(lista que no cambia de orden)
    struct entrada_tabla entradas[5];
    int son=0;
    int kill_err=0;
    //int arg=0;
    
    //numero introducido por terminal
    int n_proceso=1;
    //caracter para decidir si matar otro proceso o no
    char kill_another = ' ';

    //flag para entrar o salir del menu para matar procesos
    int menu = 0;

    //creamos los hijos
    for(int i=0;i<5;i++){
        if(getpid()==father){
            pid_son[i]=fork();
            pid=pid_son[i];
            //inicializamos los pids iniciales en orden segun argumento
            entradas[i].pid = pid_son[i];
            entradas[i].arg = i;
        }
    }

    //iniciamos los procesos de los hijos
    if(pid==0){
        execvp("./proceso",argv);
        _exit(EXIT_FAILURE);
    }

    //paramos todos los hijos antes de empezar
    //No sabemos por qué si hacemos el SIGSTOP dentro de un bucle for, no lo coge
    kill_err=kill(pid_son[0],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    kill_err=kill(pid_son[1],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    kill_err=kill(pid_son[2],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    kill_err=kill(pid_son[3],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    kill_err=kill(pid_son[4],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    
    //asignamos acciones a las señales con el handler
    if (signal(SIGALRM,signal_handler)==SIG_ERR){
        perror(strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (signal(SIGINT,signal_handler)==SIG_ERR){
        perror(strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (signal(SIGQUIT,signal_handler)==SIG_ERR){
        perror(strerror(errno));
        exit(EXIT_FAILURE);
    }

    //inicio while infinito hasta matae todos los procesos o hacer ctrl+C
    while(1){
        //actualizar iteraciones
        update_iter(entradas, pid_son[son]);
        //guardamos el que se está ejecutando
        pid_last = pid_son[son];
        //mandamos continuar al siguiente
        kill_err=kill(pid_son[son],SIGCONT);
        if(kill_err<0){
            perror(strerror(errno));
        }

        alarm(1);
        pause();
        alarm(0);
        kill_err=kill(pid_son[son],SIGSTOP);
        if(kill_err<0){
            perror(strerror(errno));
        }

        //si hacemos CTRL+"\"
        if(quit==1){
            quit = 0;
                //PASO 1
            //paramos el que se ejecuta ahora mismo
            kill_err=kill(pid_son[son],SIGSTOP);
            
            while(menu == 0){
                    //PASO 2
                //revisar cuales estan muertos y actualizar la informacion 
                for(int i=0;i<5;i++){
                    //actualizar status
                    if(!isdead(entradas[i].pid, dead_son)){
                        //-1 significa que no ha terminado(esta vivo)
                        entradas[i].status = -1;
                    }else{
                        entradas[i].status = status[i];
                    }

                    //actualizar iteraciones
                    entradas[i].ejec = iter_sons[i];

                    //actualizar last status
                    //si esta muerto = DEAD
                    if (isdead(entradas[i].pid, dead_son)){
                        strcpy(entradas[i].last_status, "DEAD   ");
                    //si esta vivo y se esta ejecutando = CURRENT
                    }else if(entradas[i].pid==pid_last){
                        strcpy(entradas[i].last_status,"CURRENT");
                    //si vivo pero no se esta ejecutando = READY
                    }else{
                        strcpy(entradas[i].last_status, "READY  ");
                    }                
                }
                
                //print de toda la informacion
                printf("\narg   pid    Last status   nº ejecuciones    status\n");
                for (int i=0;i<5;i++){
                    printf("%d     %d   %s         %d              %d\n",
                    entradas[i].arg, entradas[i].pid, entradas[i].last_status, 
                    entradas[i].ejec, entradas[i].status);
                }
                    //PASO 3
                //pedir numero entre 1 y 5
                /*
                Tenemos un problema con el scanf()y getchar() que usamos, el primero funciona
                correctamente, pero el segundo (yes o no) parece que vuelve a leer lo
                que se introdujo en el primero (un numero) y da por incorrecto el argumento,
                pero si se introduce 'y' o 'n' funciona correctamente.
                Solamente imprime el mensaje de error y la petición dos veces.
                */
                int ok = 0;
                //pedimos un numero entre 1 y 5
                while(ok == 0){
                    printf("Introduzca un numero entre 1 y 5: \n");
                    scanf(" %d", &n_proceso);
                    fflush(stdin);
                    if(n_proceso<1 || n_proceso>5){
                        printf("Valor no aceptado.\n");
                    }else{
                        ok = 1;
                    }
                }
                    //PASO 4
                //proceso ya finalizado (DEAD)
                if(isdead(entradas[n_proceso-1].pid, dead_son)){
                    printf("El proceso: %d  ya está muerto\n", n_proceso);
                
                    //PASO 5
                }else {
                    //matamos el proceso
                    kill_err=kill(entradas[n_proceso-1].pid,SIGKILL);
                    if(kill_err<0){
                        perror(strerror(errno));
                    }
                    //actualizar el numero de muertos
                    ndead++;
                    //actualizar dead_son
                    insertdead(entradas[n_proceso-1].pid, dead_son);
    
                    //actualizar el orden de ejecucion de los procesos
                    reorder(dead_son, pid_son);
                            
                    //si todos los procesos ya estan muertos                    
                    if(alldead(dead_son)){
                        printf("Se ha matado al ultimo proceso vivo\n");
                        //en este caso terminaremos en el PASO 6

                    //si el que matamos es el CURRENT
                    }else if(entradas[n_proceso-1].pid == pid_last){

                        if(isdead(pid_son[son], dead_son)){
                            son = updatecurrent(son, n_proceso, pid_son[son], entradas);
                        }

                        pid_last = pid_son[son];
                    //si el que matamos no es current
                    }else{
                        /*
                        como reordenamos pid_son, si eliminamos un proceso
                        con posicion anterior al actual CURRENT, movemos
                        este proceso al final de pid_son, por lo que hay
                        que cambiar el indice de son
                        */
                        if(entradas[n_proceso-1].pid < pid_last){
                            son = son-1;
                        }
                    }
                }
                    //PASO 6
                //preguntar si se quiere eliminar otro proceso
                printf("Quiere eliminar otro proceso?\n");
                ok = 0;
                while(ok == 0){
                    printf("yes[y]  no[n]: \n");
                    fflush(stdin);
                    kill_another = getchar();
                    fflush(stdin);
                    if(kill_another!='y' && kill_another!='n'){
                        printf("Valor no aceptado.\n");
                    }else{
                        ok = 1;
                    }
                }

                //no
                if(kill_another == 'n'){
                    //si todos están muertos
                    if(alldead(dead_son)){
                        printf("Ya están todos muertos\n");
                        //terminar el programa
                        exit(0);
                    }
                    
                    //continua el proceso CURRENT
                    kill_err=kill(pid_son[son],SIGCONT);
                    if(kill_err<0){
                        perror(strerror(errno));
                    }
                    alarm(1);
                    pause();
                    alarm(0);

                    menu = 1;
                    
                }
                //yes -> continuar a PASO 2
            }
            menu=0;
        }

        //si hacemos CTRL + C
        if(fin==1){
            //matamos los cinco procesos
            //revisando los que ya estan muertos

            if(!isdead(entradas[0].pid,dead_son)){
                kill_err=kill(pid_son[0],SIGKILL);
                if(kill_err<0) perror(strerror(errno));
                pid_fin[0]=wait(&status[0]);
            }

            if(!isdead(entradas[1].pid,dead_son)){
                kill_err=kill(pid_son[1],SIGKILL);
                if(kill_err<0) perror(strerror(errno));
                pid_fin[1]=wait(&status[1]);
            }

            if(!isdead(entradas[2].pid,dead_son)){
                kill_err=kill(pid_son[2],SIGKILL);
                if(kill_err<0) perror(strerror(errno));
                pid_fin[2]=wait(&status[2]);
            }

            if(!isdead(entradas[3].pid,dead_son)){
                kill_err=kill(pid_son[3],SIGKILL);
                if(kill_err<0) perror(strerror(errno));
                pid_fin[3]=wait(&status[3]);
            }

            if(!isdead(entradas[4].pid,dead_son)){
                kill_err=kill(pid_son[4],SIGKILL);
                if(kill_err<0) perror(strerror(errno));
                pid_fin[4]=wait(&status[4]);
            }
            //guardamos la informacion en un array de structs
            
            for (int i = 0; i < 5; i++) {
                entradas[i].ejec=iter_sons[i];
                if(!isdead(entradas[i].pid, dead_son)){
                    entradas[i].status=WEXITSTATUS(status[i]);
                }else{
                    entradas[i].status=-1;

                }
                
                //vivo y es el que estaba en ejecucion
                if((!isdead(entradas[i].pid,dead_son)) && entradas[i].pid == pid_last){
                    strcpy(entradas[i].last_status, "RUNNING");
                //vivo y en espera
                }else if(!isdead(entradas[i].pid,dead_son)){
                    strcpy(entradas[i].last_status, "READY  ");
                //muerto
                }else{
                    strcpy(entradas[i].last_status, "DEAD   ");
                }
                //arg++;
            }

            if((fp = fopen(FIFO_FILE, "w")) == NULL) {
                perror("fopen");
                exit(EXIT_FAILURE);
            }
            //mandamos a stats_process.c los datos 
            fwrite(&entradas,sizeof(struct entrada_tabla),5,fp);
            if (fp != NULL) fclose(fp);
      
            exit(0);
        }
        //actualizamos indices del orden para ejecutar los procesos
        if(son==(4-ndead)){
            son=0;
        }else{
            son++;
        }
    }
}

//handler de las señales
void signal_handler(int signo){
    if(signo==SIGALRM){
        alarma++;
    }
    if(signo==SIGINT){
        fin=1;
    }
    if(signo == SIGQUIT){
        quit=1;
    }
}

//devuelve true si el proceso está muerto
bool isdead(pid_t pid, pid_t dead_son[5]){
    bool isdead=false;
    for(int i=0;i<5;i++){
        if(pid==dead_son[i]){
            isdead=true;
        }
    }
    return isdead;
}

//actualizar los contadores de iteraciones para los vivos
void update_iter(struct entrada_tabla entradas[5], pid_t pid_son){
    for(int i=0;i<5;i++){
        if(entradas[i].pid==pid_son){
            iter_sons[i]++;
        }
    }    
}

//devuelve true si no quedan hijos vivos
bool alldead(pid_t dead_son[5]){
    int count = 0;
    for(int i=0;i<5;i++){
        //si es 0 es que este hijo no está muerto
        if(dead_son[i] != 0){
            count++;
        }
    }
    if (count == 5){
        return true;
    }else {
        return false;
    }
}

//reordena los procesos vivos
//quita el que acaba de morir y lo coloca al final, moviendo los posteriores una
//posicion hacia delante
void reorder(pid_t dead_son[5], pid_t pid_son[5]){
    pid_t aux[5] = {0,0,0,0,0};
    int alive = 0;
    for(int i=0;i<5;i++){
        //si esta vivo
        if(!isdead(pid_son[i], dead_son)){
            aux[alive]=pid_son[i];
            alive++;
        }
    }
    for(int i=alive;i<5;i++){
        aux[i] = dead_son[i-alive];
    }
    for(int i=0;i<5;i++){
        pid_son[i] = aux[i];
    }
}

//coloca en dead_son los hijos que van muriendo 
void insertdead(pid_t pid, pid_t dead_son[5]){
    int inserted = 0;
    for(int i=0;i<5;i++){
        if(inserted == 0){
            if(dead_son[i]==0){
                dead_son[i] = pid;
                inserted = 1;
            }
        }
    }
}

//devuelve el indice en pid_son del siguiente proceso a ejecutar
int updatecurrent(int son, int n_proceso, pid_t pid_son, struct entrada_tabla entradas[5]){
    int updated=0;
    int index = (int)pid_son;
    while(updated==0){
        if(exists(index+1, entradas)){
            if(!isdead(index+1, dead_son)){
                son = searchindex(pid_son+1);
                updated=1;
            }else{
                index++;
            }
        }else{
            index = index-5;
        }
    }
    return son;
}

//buscar si un pid existe en nuestro struct de entradas
bool exists(pid_t pid, struct entrada_tabla entradas[5]){
    bool found=false;

    for(int i=0; i<5;i++){
        //si existe en entradas
        if((pid) == entradas[i].pid){
               found = true;
        }
    }
    return found;
}

//dado un pid, que posicion ocupa en pid_son (dentro de los que están vivos)
int searchindex(pid_t pid){
    int devolver =0;
    for(int i=0; i<(5-ndead);i++){
        if(pid == pid_son[i]) devolver = i;
    }
    return devolver;
}
