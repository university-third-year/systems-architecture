
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>

//void alarm_handler(int signo);

int main(int argc, char **argv){
  pid_t pid=0;
  pid_t father=getpid();
  pid_t pid_son[5];
  int son=0;
  //int iter=0;
  int next_son=0;
  int kill_err=0;
  for(int i=0;i<5;i++){
    if(getpid()==father){
      pid_son[i]=fork();
      pid=pid_son[i];
    }
  }
  if(pid==0){
    execvp("./proceso",argv);
  }
  while(1){
    if(getpid()==father){
      //alarm(1);
      //pause();
      kill_err=kill(pid_son[son],SIGSTOP);
      if(kill_err<0){
        perror(strerror(errno));
      }
      kill_err=kill(pid_son[next_son],SIGCONT);
      if(kill_err<0){
        perror(strerror(errno));
      }
    }
    if(son==4){
      son=0;
    }else{
      son++;
    }
    if(next_son==4){
      next_son=0;
    }else{
      next_son++;
    }
    alarm(1);
    pause();
  }
}

/*void alarm_handler(int signo){
  if(signo==SIGALRM){
    kill_err=kill(pid_fin[son],SIGSTOP);
      if(kill_err<0){
        perror(strerror(errno));
      }
  }
}*/


