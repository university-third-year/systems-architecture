#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>

bool isNumber(char number[]);

int main(int argc, char **argv){


    int num = 0; //argumento de entrada
    int iter = 0;//numero de iteraciones
    int i;

    //revisar numero de argumentos
    if(argc != 2){
        printf("ERROR numero erroneo de argumentos\n ./proceso <<int numero>>\n");
        exit(0);
    }

    //pasar num a de string a int
    num = atoi(argv[1]);

    //error en el argumento
    if (num < 0 || !isNumber(argv[1])){
        printf("error, introduzca un numero mayor que 0 o que incluya únicamente números.\n");
        exit(0);
    }


    while(1){   //bucle infinito de prints           // sino usar iter<5
        printf("[%i] Proceso %d ", getpid(), num); //Se imprime el número de proceso y el número de argv[1]
        for(i=0; i<num; i++){
            printf("*");
        }
        printf(" iter %d\n", iter+1);//se imprime el número de iteración

        iter++;
    }

}

bool isNumber(char number[]){
    int i = 0;
    if (number[0] == '-') i = 1;
    for (; number[i] != 0; i++) {
        if (!isdigit(number[i])) return false;
    }
   return true;
}


