#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>

void alarm_handler(int signo);

int main(int argc, char **argv){
  pid_t pid=0;
  pid_t father=getpid();
  pid_t pid_son[5]={0,0,0,0,0};
  int son=0;
  int kill_err=0;
  //creacion de los hijos
  for(int i=0;i<5;i++){
    if(getpid()==father){
      pid_son[i]=fork();
      pid=pid_son[i];
    }
  }
  //proceso hijo
  if(pid==0){
    execvp("./proceso",argv);
    _exit(EXIT_FAILURE);
  }
  //paramos todos los procesos
    kill_err=kill(pid_son[0],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    kill_err=kill(pid_son[1],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    kill_err=kill(pid_son[2],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    kill_err=kill(pid_son[3],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    kill_err=kill(pid_son[4],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
 //No sabemos por qué si gacemos el SIGSTOP dentro de un bucle for, no lo coge

 //asignamos una accion a SIGALARM
  if (signal(SIGALRM,alarm_handler)==SIG_ERR){
    perror(strerror(errno));
    exit(EXIT_FAILURE);
  }
  //bucle en el que hacemos ROUND ROBIN
  while(1){
    //mandamos continuar al que le toca
    //inicialmente el hijo 0 (son = 0)
      kill_err=kill(pid_son[son],SIGCONT);
      if(kill_err<0){
        perror(strerror(errno));
      }
      //activamos la alarma para 1 segundo y esperamos
      alarm(1);
      pause();

      //paramso este proceso
      kill_err=kill(pid_son[son],SIGSTOP);
      if(kill_err<0){
        perror(strerror(errno));
      }
    //actualizamos el indice
    if(son==4){
      son=0;
    }else{
      son++;
    }
  }
}

void alarm_handler(int signo){

}




