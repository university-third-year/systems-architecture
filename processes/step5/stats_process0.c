#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/stat.h>
#include <linux/stat.h>
 
#define FIFO_FILE "MYFIFO"
 
struct entrada_tabla{
    int arg;
    pid_t pid;
    int last_status;
    int ejec;
    int status;
};
 
int main(void){

    FILE *fp;
    //creamos el fifo
    umask(0);
    mknod(FIFO_FILE, S_IFIFO|0666, 0);

    struct entrada_tabla entradas[5];
    size_t num_bytes;
    //abrimos el fifo para lectura
    if((fp = fopen(FIFO_FILE, "r")) == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    
    //leemos del fifo
    num_bytes = fread(&entradas,sizeof(struct entrada_tabla),5,fp);
          
    //cuando se escriba, recibimos la información y hacemos print
    if (num_bytes != 0){
        printf("\narg   pid    Last status   nº ejecuciones    status\n");
        for (int i=0;i<5;i++){
            if(entradas[i].last_status==entradas[i].arg){
                printf("%d     %d   RUNNING       %d                 %d\n",entradas[i].arg, entradas[i].pid,entradas[i].ejec,entradas[i].status);
            }else{
                printf("%d     %d   READY         %d                 %d\n",entradas[i].arg, entradas[i].pid,entradas[i].ejec,entradas[i].status);               
            }
        }
    }
    //cerramos el descriptor de fichero y terminamos
    fclose(fp);
    exit(EXIT_SUCCESS);
}
 
 
 
 

