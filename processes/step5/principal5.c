#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>

#define FIFO_FILE "MYFIFO"

struct entrada_tabla{
    int arg;
    pid_t pid;
    int last_status;
    int ejec;
    int status;
};

void signal_handler(int signo);
int calc_iter(int iteraciones,int arg);

int alarma=0;
int fin=0;

int main(int argc, char **argv){

    FILE *fp;

    pid_t pid=0;
    pid_t father=getpid();
    pid_t pid_son[5]={0,0,0,0,0};
    int pid_fin[5]={0,0,0,0,0};
    int status[5]={0,0,0,0,0};
    struct entrada_tabla entradas[5];
    int son=0;
    int iter=0;
    int kill_err=0;
    int arg=0;

    for(int i=0;i<5;i++){
        if(getpid()==father){
            pid_son[i]=fork();
            pid=pid_son[i];
        }
    }

    if(pid==0){
        execvp("./proceso",argv);
        _exit(EXIT_FAILURE);
    }

    //paramos todos los hijos antes de empezar
    //No sabemos por qué si hacemos el SIGSTOP dentro de un bucle for, no lo coge
    kill_err=kill(pid_son[0],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    kill_err=kill(pid_son[1],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    kill_err=kill(pid_son[2],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    kill_err=kill(pid_son[3],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    kill_err=kill(pid_son[4],SIGSTOP);
    if(kill_err<0){
      perror(strerror(errno));
    }
    
    if (signal(SIGALRM,signal_handler)==SIG_ERR){
        perror(strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (signal(SIGINT,signal_handler)==SIG_ERR){
        perror(strerror(errno));
        exit(EXIT_FAILURE);
    }

    while(1){
        //continua el proceso al que le toca
        kill_err=kill(pid_son[son],SIGCONT);
        iter++;
        if(kill_err<0){
            perror(strerror(errno));
        }

        alarm(1);
        pause();
        alarm(0);
        //una vez pasa un segundo sin saltar la alarma, paramos el proceso en ejecucion
        kill_err=kill(pid_son[son],SIGSTOP);
        if(kill_err<0){
            perror(strerror(errno));
        }

        //actualizamos el indice del array de pids: pid_son
        if(son==4){
            son=0;
        }else{
            son++;
        }

        //si hacemos CTRL + C
        if(fin==1){
            //matamos los cinco procesos
            kill_err=kill(pid_son[0],SIGKILL);
            if(kill_err<0) perror(strerror(errno));
            pid_fin[0]=wait(&status[0]);

            kill_err=kill(pid_son[1],SIGKILL);
            if(kill_err<0) perror(strerror(errno));
            pid_fin[1]=wait(&status[1]);

            kill_err=kill(pid_son[2],SIGKILL);
            if(kill_err<0) perror(strerror(errno));
            pid_fin[2]=wait(&status[2]);

            kill_err=kill(pid_son[3],SIGKILL);
            if(kill_err<0) perror(strerror(errno));
            pid_fin[3]=wait(&status[3]);

            kill_err=kill(pid_son[4],SIGKILL);
            if(kill_err<0) perror(strerror(errno));
            pid_fin[4]=wait(&status[4]);

            //guardamos la informacion en un array de structs
            for (int i = 0; i < 5; i++) {
                entradas[i].arg=arg;
                entradas[i].pid=pid_son[i];
                entradas[i].last_status=(iter%5)-1;
                entradas[i].ejec=calc_iter(iter,arg);
                entradas[i].status=WEXITSTATUS(status[i]);
                arg++;
            }

            if((fp = fopen(FIFO_FILE, "w")) == NULL) {
                perror("fopen");
                exit(EXIT_FAILURE);
            }
            //mandamos a stats_process.c los datos 
            fwrite(&entradas,sizeof(struct entrada_tabla),5,fp);
            if (fp != NULL) fclose(fp);
    
            exit(0);
        }
    }
}

//handler de las señales
void signal_handler(int signo){
    if(signo==SIGALRM){
        alarma++;
    }
    if(signo==SIGINT){
        fin=1;
    }
}

//calcula cuantas iteraciones tiene cada proceso
int calc_iter(int iteraciones, int arg){
    int iter=iteraciones/5;
    if(arg<(iteraciones%5)){
        iter++;
    }
    return iter;
}
