import java.util.Random;


class Avion extends Thread{

    private int id;
    Aeropuerto aero;
    private int apistaAsig=0; 
    Random rnd = new Random();

    Avion(int id, Aeropuerto aero){
        this.id=id;
        this.aero=aero;
    }


    public void run() {

        System.out.println("["+id+"] Antes del espacio aereo");

        //asignar a que pista esperar
        apistaAsig=aero.asignarCola();


        if(apistaAsig==1) { //pista A asignada

            aero.inespacioAereo();

            System.out.println("["+id+"] En el espacio aereo. Pista asignada: A");

            aero.inpistaA();
            aero.outespacioAereo();

            System.out.println("["+id+"] En pista A aterrizando");
            try { Thread.sleep(rnd.nextInt(50)); } catch (Exception e1) {}//tiempo en pista

            aero.outpistaA();
            
            aero.outcolaA();

            
        }else { //pista B asignada

            aero.inespacioAereo();

            System.out.println("["+id+"] En el espacio aereo. Pista asignada: B");

            aero.inpistaB();
            aero.outespacioAereo();

            System.out.println("["+id+"] En pista B aterrizando");
            try { Thread.sleep(rnd.nextInt(50)); } catch (Exception e1) {}//tiempo en pista

            aero.outpistaB();
            
            aero.outcolaB();

        }
        
        //EN EL HANGAR
        System.out.println("["+id+"] Entra al Hangar");
        try {Thread.sleep(rnd.nextInt(500)); } catch (Exception e1) {} //tiempo en hangar

        
        //ASIGNAMOS COLA DE NUEVO
        apistaAsig=0;
        while(apistaAsig==0) {
            apistaAsig=aero.asignarCola();
        }

        //DESPEGUE
        if(apistaAsig==1) { //pista A asignada
            

            System.out.println("["+id+"] En cola A para despegar");
            aero.inespacioAereo();
            aero.inpistaA();

            System.out.println("["+id+"] En pista A para despegar");
            try { Thread.sleep(rnd.nextInt(50)); } catch (Exception e1) {}//tiempo en pista

            aero.outpistaA();
            
            aero.outcolaA();

            System.out.println("["+id+"] En Espacio Aereo Despegando");
            try { Thread.sleep(rnd.nextInt(10)); } catch (Exception e1) {} //tiempo despegando

            aero.outespacioAereo();

            System.out.println("["+id+"] Y se marcho...");



        }else { //pista B asignada

            System.out.println("["+id+"] En cola B para despegar");
            aero.inespacioAereo();
            aero.inpistaB();

            System.out.println("["+id+"] En pista B para despegar");
            try { Thread.sleep(rnd.nextInt(50)); } catch (Exception e1) {}//tiempo en pista

            
            aero.outpistaB();
            
            aero.outcolaB();

            System.out.println("["+id+"] En Espacio Aereo Despegando");
            try { Thread.sleep(rnd.nextInt(10)); } catch (Exception e1) {} //tiempo despegando

            aero.outespacioAereo();

            System.out.println("["+id+"] Y se marcho...");

        }
    }
}



class Aeropuerto {
    private int avionesA; //aviones en cola A
    private int avionesB; //aviones en cola B
    private int avionesE; //aviones en espacio aereo
    private boolean ocupadaA=false; //pista A ocupada
    private boolean ocupadaB=false; //pista B ocupada

    int M;
    int N;

    Aeropuerto(int M, int N){
        this.M=M;
        this.N=N;
    }


    public synchronized int asignarCola() {

        int asignacion = 0;
        while(avionesA==M && avionesB==M) {
            try { this.wait(); } catch (Exception e) {}
        }

        if(avionesA<=avionesB) {
            asignacion = 1;
            avionesA++;
        }else{
            asignacion = 2;
            avionesB++;
        }
        return asignacion;
    }


    public synchronized void inespacioAereo() {
        while(avionesE == N) {
            try { this.wait(); } catch (Exception e) {}
        }
        avionesE++;
    }

    public synchronized void outespacioAereo() {
        avionesE--;
        this.notifyAll(); //avisamos a los que estan en pista para despegar y a los que quieren aterrizar
    }
    
    
    public synchronized void outcolaA() {
        avionesA--;
        this.notifyAll();
    }

    public synchronized void outcolaB() {
        avionesB--;
        this.notifyAll();
    }
    
    public synchronized void inpistaA() {
        while(ocupadaA) {
            try { this.wait(); } catch (Exception e) {}
        }
        ocupadaA=true;
    }

    public synchronized void inpistaB() {
        while(ocupadaB) {
            try { this.wait(); } catch (Exception e) {}
        }
        ocupadaB=true;
    }
    
    public synchronized void outpistaA() {
        ocupadaA=false;
        this.notifyAll(); //avisamos a cola A
    }

    public synchronized void outpistaB() {
        ocupadaB=false;
        this.notifyAll(); //avisamos a cola B
    }
}


public class Aeropuerto_m {

    public static void main(String args[]) {
        
        
        if(args.length != 2){
            System.out.println("ERROR, solo 3 argumentos");
            System.out.println("java Aeropuerto_S <M> <N>");
            System.exit(0);
        }

        
        int M = Integer.parseInt(args[0]);
        int N = Integer.parseInt(args[1]);
        

        Aeropuerto aero = new Aeropuerto(M, N);

        Avion a;

        for(int i=0; i<20; i++) {
            a = new Avion(i, aero);
            a.start();
        }
    }
}


