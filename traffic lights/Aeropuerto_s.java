import java.lang.*;
import java.util.*;
import java.util.concurrent.Semaphore;


class Avion extends Thread{

    private static int avionesA; //n aviones en la cola de la pista A
    private static int avionesB; //n aviones en la cola de la pista B
    
    private char pista;
    
    private int M,N;

    //private Semaphore mutexA, mutexB, colaA, colaB, espacioAereo;
    Semaphore mutex, cola, espacioAereo, pistaA, pistaB;

    Random rnd = new Random();

    private int id;

    public Avion(int id, int M, int N, Semaphore mutex, Semaphore cola, Semaphore pistaA, Semaphore pistaB, Semaphore espacioAereo){
        this.id=id;
        this.M=M;
        this.N=N;
        this.mutex=mutex;
        this.cola=cola;
        this.pistaA=pistaA;
        this.pistaB=pistaB;
        this.espacioAereo=espacioAereo;
    }

    public void run(){

        //ATERRIZAR
        System.out.println("["+id+"] Antes del espacio aereo");

        //ASIGNAR PISTA
        try{ cola.acquire(); }catch(Exception e){}
        try{ mutex.acquire(); }catch(Exception e){}
        
        if(avionesA<=avionesB) {
            pista = 'A';
            avionesA++;
        }else {
            pista = 'B';
            avionesB++;
        }
        mutex.release();
        
        //ATERRIZAMOS EN PISTA A
        if(pista == 'A'){
                        
            try{ espacioAereo.acquire(); }catch(Exception e){} // entramos fisicamente en el espacio aereo
            System.out.println("["+id+"] En el espacio aereo. Pista asignada: A");
            
            try{ pistaA.acquire(); }catch(Exception e){}//entramos en pista A
            espacioAereo.release(); //salimos del espacio aereo
            System.out.println("["+id+"] Aterriza en pista A");

                cola.release(); //salimos de cola A
                try{ mutex.acquire(); }catch(Exception e){}
                System.out.println("["+id+"] Abandona cola de A");
                avionesA--;
                mutex.release();
            
            try { Thread.sleep(rnd.nextInt(50)); } catch (Exception e1) {}//tiempo en pista
            
            pistaA.release(); //salimos de la pista A
            System.out.println("["+id+"] Abandona pista A");

        //ATERRIZAMOS EN PISTA B
        }else{
                        
            try{ espacioAereo.acquire(); }catch(Exception e){} // entramos fisicamente en el espacio aereo
            System.out.println("["+id+"] En el espacio aereo. Pista asignada: B");
            
            try{ pistaB.acquire(); }catch(Exception e){}//entramos en pista B
            espacioAereo.release(); //salimos del espacio aereo
            System.out.println("["+id+"] Aterriza en pista B");

                cola.release(); //salimos de cola B
                try{ mutex.acquire(); }catch(Exception e){}
                System.out.println("["+id+"] Abandona cola de B");
                avionesB--;
                mutex.release();
            
            try { Thread.sleep(rnd.nextInt(50)); } catch (Exception e1) {}//tiempo en pista
            
            pistaB.release(); //salimos de la pista B
            System.out.println("["+id+"] Abandona pista B");
        }
        

        //ENTRAR AL HANGAR
        System.out.println("["+id+"] Entra al Hangar");
        try {Thread.sleep(rnd.nextInt(500)); } catch (Exception e1) {} //tiempo en hangar


        //DESPEGAR
        System.out.println("["+id+"] Quiero despegar");
        
        //ASIGNAMOS PISTA
        try{ cola.acquire(); }catch(Exception e){}
        try{ mutex.acquire(); }catch(Exception e){}
        
        if(avionesA<=avionesB) {
            pista = 'A';
            avionesA++;
        }else {
            pista = 'B';
            avionesB++;
        }
        mutex.release();
        
        //DESPEGAMOS EN PISTA A
        if(pista == 'A'){ 
            
            System.out.println("["+id+"] En el Hangar. Pista asignada: A");
            try{ espacioAereo.acquire(); }catch(Exception e){} //solicitamos espacio aereo
            
            try{ pistaA.acquire(); }catch(Exception e){}//entramos en pista A
            System.out.println("["+id+"] Entra en pista A para despegar");

                cola.release(); //salimos de cola A
                try{ mutex.acquire(); }catch(Exception e){}
                System.out.println("["+id+"] Abandona cola de A");
                avionesA--;
                mutex.release();
            
            try { Thread.sleep(rnd.nextInt(50)); } catch (Exception e1) {}//tiempo en pista
            
            pistaA.release();
            System.out.println("["+id+"] En el espacio aereo");
            
            
            try { Thread.sleep(rnd.nextInt(10)); } catch (Exception e1) {}//tiempo despegando
            
            espacioAereo.release();
            System.out.println("["+id+"] Y se marchoooo...");
        
        //DESPEGAMOS EN PISTA B
        }else{ 
            
            System.out.println("["+id+"] En el Hangar. Pista asignada: B");
            try{ espacioAereo.acquire(); }catch(Exception e){} //solicitamos espacio aereo
            
            try{ pistaB.acquire(); }catch(Exception e){}//entramos en pista B
            System.out.println("["+id+"] Entra en pista B para despegar");

                cola.release(); //salimos de cola A
                try{ mutex.acquire(); }catch(Exception e){}
                System.out.println("["+id+"] Abandona cola de B");
                avionesB--;
                mutex.release();
            
            try { Thread.sleep(rnd.nextInt(50)); } catch (Exception e1) {}//tiempo en pista
            
            pistaB.release();
            System.out.println("["+id+"] En el espacio aereo");
            
            
            try { Thread.sleep(rnd.nextInt(10)); } catch (Exception e1) {}//tiempo despegando
            
            espacioAereo.release();
            System.out.println("["+id+"] Y se marchoooo...");
        }
    }
}


public class Aeropuerto_s{

    public static void main(String[] args){
        
        
        if(args.length != 2){
            System.out.println("ERROR, solo 3 argumentos");
            System.out.println("java Aeropuerto_S <M> <N>");
            System.exit(0);
        }

        
        int M = Integer.parseInt(args[0]);
        int N = Integer.parseInt(args[1]);
        

        Avion a;

        Semaphore mutex = new Semaphore(1);         // aviones en pista A y pista B  //solo un mutex
        Semaphore cola = new Semaphore(2*M);         // cola de las pistas
        Semaphore pistaA = new Semaphore(1);         // pista A
        Semaphore pistaB = new Semaphore(1);         // pista B
        Semaphore espacioAereo = new Semaphore(N);     //espacio aereo
        
        for(int i=0; i<20; i++) {
            a = new Avion(i, M, N, mutex, cola, pistaA, pistaB, espacioAereo);
            a.start();
        }
    }
}

