import requests
import json
import sys
from datetime import datetime

import math
#funcion para sacar la distancia entre dos puntos
def distancia(lat_1,lat_2,lon_1,lon_2):
    R=6372800 #radio de la tierra

    phi1=math.radians(float(lat_1))
    phi2=math.radians(float(lat_2))
    dphi=math.radians(float(lat_2)-float(lat_1))
    dlambda=math.radians(float(lon_2)-float(lon_1))

    a=math.sin(dphi/2)**2+math.cos(phi1)*math.sin(phi2)*math.sin(dlambda/2)**2

    return 2*R*math.atan2(math.sqrt(a),math.sqrt(1-a))

#funcion que genera una string a partir de la dirección para adecuarla al formato requerido para las peticiones
def montarLoc(localizacion):
    x=localizacion.split(" ")
    a=[]
    for i in range(len(x)):
        a.append(x[i].replace(',',''))
    loc=""
    for i in range(len(a)-1):
        loc=loc+a[i]+"%20"
    loc=loc+a[len(a)-1]
    return loc

#funcion que devuelve coordenadas dada una dirección
def coordinates(locat):
    #generamos la petición completa
    loc=montarLoc(locat)
    URL="https://nominatim.openstreetmap.org/search.php?q="
    options="&polygon_geojson=1&format=jsonv2"
    petition=URL+loc+options
    #hacemos la petición
    result = requests.get(petition)
    #guardamos la respuesta
    result_json=result.json()
    name=result_json[0]['display_name']
    lat_1=result_json[0]['lat']
    lon_1=result_json[0]['lon']
    #devolvemos los datos
    return name, lon_1, lat_1
    
#funcion que imprime información de los parkings mas cercanos a unas coordenadas específicas
def parkings(lon_1, lat_1):
    #obtenemos el tiempo actual
    date=time.strftime("%Y-%m-%dT%H:%M:%S")
    #generamos parametros
    paramInfoParking_={
    'tFamilyTTypeTCategory':[{'poiCategory':["001"],'poiFamily':'001','poiType':'001'}],
    'longitude': lon_1,
    'latitude':lat_1,
    'dateTimeUse':date,
    'language':'ES',
    'minimumPlacesAvailable':[5],
    'nameFieldCodes':[{'string':["001"],'nameField':'Tipo parking'}],
    'radius': 0.5
    }
    #la pasamos a formato json y construimos la petición
    paramInfoParking=json.dumps(paramInfoParking_)
    petition="https://datos.madrid.es/egob/catalogo/50027-2069717-AparcamientosOcupacionYServicios.json?paramInfoParking="+paramInfoParking
    #hacemos la petición
    result=requests.get(petition)
    #almacenamos la respuesta
    result_json=result.json()
    id=[]
    address=[]
    lon_2=[]
    lat_2=[]
    distance=[]
    #parseamos y printeamos la información
    for i in range(3):
        id.append(result_json[i]['id'])
        address.append(result_json[i]['address'])
        lon_2.append(result_json[i]['longitude'])
        lat_2.append(result_json[i]['latitude'])
        distance.append(distancia(lat_1,lat_2[i],lon_1,lon_2[i]))
    print("id-Address-coordinates-distance")
    for i in range(3):
        print(str(id[i])+"-"+address[i]+"-"+"("+str(lat_2[i])+","+str(lon_2[i])+")"+"-"+str(distance[i]))
    print("")

#funcion que devuelve la información de un parking en específico
def info_parking(id):
    #obtenemos el tiempo actual
    date2=time.strftime("%Y-%m-%dT%H")
    minute=time.strftime("%M")
    seconds=time.strftime("%S")
    #generamos la petición
    petition="https://datos.madrid.es/egob/catalogo/50027-2069413-AparcamientosOcupacionYServicios.json?id="+str(id)+"&family=001&date="+date2+"%3A"+minute+"%3A"+seconds+"&language=ES&publicData=true"
    #hacemos la petición
    result=requests.get(petition)
    #almacenamos los datos de la respuesta
    result_json=result.json()
    address=result_json['general']['address']
    lat=result_json['general']['latitude']
    lon=result_json['general']['longitude']

    features={'Name':[],'Content':[]}
    for i in result_json['lstFeatures']:
        features['Name'].append(i['name'])
        features['Content'].append(i['content'])
    rates={'Descripcion':[],'HoraInicial':[],'HoraFinal':[],'MinutosInicio':[],'MinutosFin':[],'Tarifa':[]}

    for i in result_json['lstRates']:
        rates['Descripcion'].append(i['description'])
        rates['HoraInicial'].append(i['scheduleInitial'])
        rates['HoraFinal'].append(i['scheduleEnd'])
        rates['MinutosInicio'].append(i['minutesStayInitation'])
        rates['MinutosFin'].append(i['minutesStayEnd'])
        rates['Tarifa'].append(i['rate'])

    ocupacion={'Libres':[],'Momento':[]}
    for i in result_json['lstOccupation']:
        ocupacion['Libres'].append(i['free'])
        ocupacion['Momento'].append(i['moment'])
    schedule=result_json['schedule']

    #printeamos la información en el formato indicado
    print("Address: "+address)
    print("Latitude: "+lat)
    print("Longitude: "+lon)
    print("Features:")
    for i in range(0,len(features['Name'])):
        print("   "+features['Name'][i]+": "+features['Content'][i])
    print("Rates: ")
    for i in range(0,len(rates['Descripcion'])):
        print("   "+rates['Descripcion'][i]+" ("+rates['HoraInicial'][i]+"-"+rates['HoraFinal'][i]+"): "+str(rates['MinutosInicio'][i])+"-"+str(rates['MinutosFin'][i])+" "+str(rates['Tarifa'][i])+" euros")
    for i in range(0,len(ocupacion['Libres'])):
        print(str(ocupacion['Libres'][i])+" available spaces at "+ocupacion['Momento'][i])
    print("Schedule: "+schedule)

#comprobar que el modo está correctamente introducido
if(not(sys.argv[1]=="coordinates") and not(sys.argv[1]=="parkings") and not(sys.argv[1]=="info_parking")):
    raise ValueError('Introduzca un modo correcto: coordinates|parkings|info_parking')

#necesario para obtener la fecha y hora actuales 
time=datetime.now()

#en el caso de los modos coordinates o parkings comprobamos que se introduce una dirección
if ((sys.argv[1]=="coordinates") or (sys.argv[1]=="parkings")):
    if len(sys.argv)==2:
        raise ValueError('Introduzca una direccion')

    # Módulo 1
    if(sys.argv[1]=="coordinates"):
        name = ''
        lon_1=0
        lat_1=0
        #llamamos a la función coordinates
        name, lon_1, lat_1 = coordinates(sys.argv[2])
        #imprimimos la información
        print("")
        print("coordinates: "+name)
        print("")
        print("latitud: "+lat_1)
        print("")
        print("longitud: "+lon_1)
        print("")

    # Módulo 2, parte 1
    if(sys.argv[1]=="parkings"):
        name = ''
        lon_1=0
        lat_1=0
        #llamamos a la función coordinates
        name,lon_1, lat_1 = coordinates(sys.argv[2])
        #llamamos a la función parkings pasándole como argumentos los resultados de la función anterior
        parkings(lon_1, lat_1)

# Módulo 2, parte 2
if(sys.argv[1]=="info_parking"):
    if len(sys.argv)!=3:
        raise ValueError('Introduzca el numero de identificacion del parking')
    if(not(sys.argv[2].isdigit())):
        raise ValueError('Introduzca un numero como id')
    id=sys.argv[2]
    #llamamos a la función info_parking pasándole como argumento el id del parking
    info_parking(id)
